<?php
/**
 * Template Name: Home Page Template
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0
 */

get_header();

if ( have_posts() ) {

	while ( have_posts() ) {
		the_post();
		if(CFS()->get( 'about_us_title' )) : ?>
            <section id="about" class="about" data-scroll-id="about" tabindex="-1" style="outline: currentcolor none medium;">

                <div class="container">

                    <div class="row">

                        <div class="col-md-8 col-md-offset-2">

                            <!-- Section title -->
                            <h2 class="title -black"><?php echo CFS()->get( 'about_us_title' ); ?></h2>

                            <span class="subtitle h3 -yellow"><?php echo CFS()->get( 'about_us_sub_title' ); ?></span>

                        </div>

                    </div> <!-- /.row -->

                    <div class="row box-container">
                        <?php
                        $about_us_work = CFS()->get('about_us_work');
                        $workCounter = 1;
                        $workCounterText = 'one';
                        foreach ($about_us_work as $work) {
                            if($workCounter == 2) {
                                $workCounterText = 'two';
                            } elseif ($workCounter == 3) {
                                $workCounterText = 'three';
                            }
                            ?>
                            <div class="col-sm-4">

                                <div class="about-box">

                                    <h3 class="title -<?php echo $workCounterText; ?> -black"><?php echo $work['about_us_work_title']; ?></h3>

                                    <p class="desc"><?php echo $work['about_us_work_content']; ?></p>

                                </div> <!-- /.about-box -->

                            </div>
                        <?php
                            $workCounter++;
                        } ?>

                    </div> <!-- /.row -->

                </div> <!-- /.box-container -->

            </section> <!-- /.about -->
        <?php endif; ?>

        <?php if(CFS()->get( 'portfolio_title' )) : ?>
            <section id="portfolio" class="portfolio">

                <div class="container">

                    <div class="row">

                        <div class="col-md-8 col-md-offset-2">

                            <!-- Section title -->
                            <h2 class="title -black"><?php echo CFS()->get( 'portfolio_title' ); ?></h2>

                            <span class="subtitle h3 -yellow"><?php echo CFS()->get( 'portfolio_sub_title' ); ?></span>

                        </div>

                    </div> <!-- /.row -->

                    <div class="row box-container">
                        <?php echo do_shortcode(CFS()->get( 'portfolio_post_shortcode' )); ?>
                    </div> <!-- /.row -->

                </div> <!-- /.container -->

                <p class="load-more -yellow text-center">Oh...We have more of these</p>

                <a href="javascript:void(0)" class="chevron-down">
                    <img class="icon" src="<?php echo get_template_directory_uri().'/assets/images/chevron_down_black.svg'?>" title="Load more..." alt="Chevron pointing downwards">
                    <span class="sr-only">Load more...</span>
                </a>

            </section> <!-- /.portfolio -->
        <?php endif; ?>


        <?php if(CFS()->get( 'contact_us_title' )) : ?>

        <section id="contact" class="contact">

            <div class="container">

                <div class="row">

                    <div class="col-md-8 col-md-offset-2">

                        <!-- Section title -->
                        <h2 class="title -black"><?php echo CFS()->get( 'contact_us_title' ); ?></h2>

                        <span class="subtitle h3 -yellow"><?php echo CFS()->get( 'contact_us_sub_title' ); ?></span>

                    </div>

                </div> <!-- /.row -->

                <div class="row box-container">

                    <div class="col-md-10 col-md-offset-1">
                        <?php echo do_shortcode('[contact-form-7 id="9" title="Contact form 1" html_class="contact-form"]'); ?>

                    </div>

                </div> <!-- /.row -->

            </div> <!-- /.container -->

        </section> <!-- /.contact -->
        <?php endif; ?>

<?php
	}
}
get_footer();

