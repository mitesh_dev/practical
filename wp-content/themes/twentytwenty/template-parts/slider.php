<?php
/**
 * The default template for displaying content
 *
 * Used for both singular and index.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

// First lets set some arguments for the query:
// Optionally, those could of course go directly into the query,
// especially, if you have no others but post type.

$args = array(
    'post_type' => 'slider',
    'posts_per_page' => (int) $limit
    // Several more arguments could go here. Last one without a comma.
);

// Query the posts:
$slider_query = new WP_Query($args); ?>
<?php if ($slider_query->have_posts()) : ?>

<section id="home" class="home">
    <?php
        while ($slider_query->have_posts()) : $slider_query->the_post();
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full-post-thumbnail' );
        $link = CFS()->get('slider_link');
    ?>
    <div class="l-align-both">
        <?php if($image): ?>
        <figure class="logo">
            <img src="<?php echo $image[0]; ?>" title="<?php echo get_the_title(); ?>" alt="<?php echo get_the_title(); ?>">
        </figure>
        <?php endif; ?>

        <span class="h3 -white"><?php echo CFS()->get('slider_sub_title'); ?></span>

        <span class="h1 -yellow"><?php echo get_the_title(); ?></span>

        <?php if($link): ?>
            <a href="<?php echo ($link['url']) ? $link['url'] : 'javascript:void(0)'; ?>" class="button" target="<?php echo ($link['target']) ? $link['target'] : '_self'?>"><?php echo ($link['text']) ? $link['text'] : 'Read More'?></a>
        <?php endif; ?>
        <a href="#about" class="chevron-down smoothScroll">
            <img class="icon" src="<?php echo get_template_directory_uri().'/assets/images/chevron_down_white.svg'; ?>" title="Scroll down" alt="Chevron pointing downwards">
            <span class="sr-only">Scroll down</span>
        </a>

    </div>
    <?php
    endwhile;
    // Reset Post Data
    wp_reset_postdata();
    ?>
</section>
<?php endif; ?>