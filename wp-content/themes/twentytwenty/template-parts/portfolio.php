<?php

$args = array(
    'post_type' => 'portfolio',
    'posts_per_page' => (int) $limit
    // Several more arguments could go here. Last one without a comma.
);

// Query the posts:
$portfolio_query = new WP_Query($args); ?>
<?php if ($portfolio_query->have_posts()) : ?>

    <?php
        while ($portfolio_query->have_posts()) : $portfolio_query->the_post();
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full-post-thumbnail' );
    ?>
    <div class="col-xxs-12 col-xs-6 col-md-3">

        <div class="portfolio-item">
            <?php if($image): ?>
                <img src="<?php echo $image[0]; ?>" title="<?php echo get_the_title(); ?>" alt="<?php echo get_the_title(); ?>">
            <?php else: ?>
                <img src="https://via.placeholder.com/270x200.png/09f/fff" title="<?php echo get_the_title(); ?>" alt="<?php echo get_the_title(); ?>">
            <?php endif; ?>

            <div class="overlay">
                <div class="content">
                    <span class="h2 -black text-center"><?php echo get_the_title() ?></span>
                    <span class="h3 -white text-center"><?php echo get_the_content() ?></span>
                </div>
            </div> <!-- /.overlay -->

        </div> <!-- /.portfolio-item -->

    </div>
    <?php
        endwhile;
        // Reset Post Data
        wp_reset_postdata();
    ?>
<?php endif; ?>

