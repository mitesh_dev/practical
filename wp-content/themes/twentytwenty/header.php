<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?><!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >

		<link rel="profile" href="https://gmpg.org/xfn/11">

		<?php wp_head(); ?>

	</head>

	<body <?php body_class(); ?>>

    <main>
        <?php echo do_shortcode('[homepage_slider_shortcode]'); ?>
        <div class="nav-container">

            <nav id="main-navigation" class="navbar navbar-default affix-top">
                <!-- Section title -->
                <h2 class="sr-only">Main navigation</h2>

                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar" aria-expanded="false">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="sr-only">Toggle navigation</span>
                        </button>
                        <a class="navbar-brand smoothScroll" href="#home">
                            <img class="logo" src="<?php echo get_template_directory_uri().'/assets/images/yellowmoon_logo_02.svg'?>" title="YellowMoon Logo" alt="Cartoon illustration of the moon and brand title">
                            <span class="sr-only">YellowMoon Logo</span>
                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div id="main-navbar" class="collapse navbar-collapse">

                        <ul class="nav navbar-nav navbar-right">
                            <?php
                            if ( has_nav_menu( 'primary' ) ) {
                                wp_nav_menu(
                                    array(
                                        'container'  => '',
                                        'items_wrap' => '%3$s',
                                        'theme_location' => 'primary',
                                        'add_li_class'  => 'nav-item',
                                        'add_a_class'  => 'smooth-scroll'
                                    )
                                );
                            }
                            ?>
                        </ul>

                    </div> <!-- /.navbar-collapse -->

                </div> <!-- /.container -->

            </nav> <!-- /.navbar -->

        </div> <!-- /.nav-container -->


		<?php
		// Output the menu modal.
		get_template_part( 'template-parts/modal-menu' );
